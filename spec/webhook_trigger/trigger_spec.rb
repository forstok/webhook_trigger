# frozen_string_literal: true

require 'webhook_trigger/trigger'
RSpec.describe WebhookTrigger::Trigger do
  describe '.get_webhooks' do
    it 'call webhook_list in trigger_repository' do
      expect(WebhookTrigger::Repository::TriggerRepository).to receive(:webhook_list).with('event',1)
      subject.get_webhooks('event',1)
    end
  end
  describe '.perform' do
    let(:webhook) { double 'webhook' }
    let(:webhook_list) { [webhook] }
    it 'call new in configuration' do
      allow(webhook).to receive(:[]).with('name')
      allow(webhook).to receive(:[]).with('url')
      allow(subject).to receive(:get_webhooks).with('a', 2).and_return(webhook_list)
      expect(WebhookTrigger::Repository::TriggerRepository).to receive(:webhook_insert_pending).with(for: 'webhooks').with(webhook['name'], webhook['url'], 1)
      subject.perform('a',1,2)
    end
  end
end
