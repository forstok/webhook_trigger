# frozen_string_literal: true

require 'webhook_trigger/repository/trigger_repository'
RSpec.describe WebhookTrigger::Repository::TriggerRepository do
  describe '.webhook_list_query_join' do
    it 'return query join for webhooks' do
      allow(WebhookTrigger).to receive_message_chain(:configuration, :list).and_return('list')
      allow(WebhookTrigger).to receive_message_chain(:configuration, :event).and_return('event')
      expect(subject.webhook_list_query_join).to eql(' FROM list JOIN event on list.event_id=event.id ')
      subject.webhook_list_query_join
    end
  end
  describe '.webhook_list_query_select' do
    it 'return query select for webhooks' do
      allow(WebhookTrigger).to receive_message_chain(:configuration, :list).and_return('list')
      allow(WebhookTrigger).to receive_message_chain(:configuration, :event).and_return('event')
      expect(subject.webhook_list_query_select).to eql('SELECT list.id, list.url, event.name  FROM list JOIN event on list.event_id=event.id ')
      subject.webhook_list_query_select
    end
  end
  describe '.webhook_list_query_condition' do
    it 'return query condition for webhooks' do
      allow(WebhookTrigger).to receive_message_chain(:configuration, :list).and_return('list')
      allow(WebhookTrigger).to receive_message_chain(:configuration, :event).and_return('event')
      expect(subject.webhook_list_query_condition('event', 1)).to eq("WHERE list.account_id=1 AND event.name like 'event'")
      subject.webhook_list_query_condition('event', 1)
    end
  end
  describe '.webhook_list' do
    let(:sql) { double 'query insert' }
    it 'return webhooks' do
      allow(WebhookTrigger::Repository).to receive_message_chain(:client, :query).and_return('array')
      expect(WebhookTrigger::Repository.client.query(sql)).to eq('array')
    end
  end
  describe '.webhook_insert_query' do
    it 'return query insert for webhooks' do
      allow(WebhookTrigger).to receive_message_chain(:configuration, :db).and_return('db')
      allow(WebhookTrigger).to receive_message_chain(:configuration, :buffer_table).and_return('buffer_table')
      expect(subject.webhook_insert_query).to eql('INSERT INTO db.buffer_table(url, event, event_source_id) VALUES ')
      subject.webhook_insert_query
    end
  end
  describe '.webhook_insert_value' do
    it 'return query insert for webhooks' do
      expect(subject.webhook_insert_value('event', 'https://google.com', 2)).to eq("('https://google.com', 'event', 2)")
      subject.webhook_insert_value('event', 'https://google.com', 2)
    end
  end
  describe '.webhook_insert_pending' do
    let(:sql) { double 'query insert' }
    it 'insert webhook' do
      allow(WebhookTrigger::Repository).to receive_message_chain(:client, :query).and_return('object')
      expect(WebhookTrigger::Repository.client.query(sql)).to eq('object')
    end
  end
end
