# frozen_string_literal: true

require 'webhook_trigger/configuration.rb'
# Module WebhookTrigger is main module for gem webhook_trigger
module WebhookTrigger
  VERSION = '0.2.1'
end
