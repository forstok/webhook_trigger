# frozen_string_literal: true

require 'mysql2'
module WebhookTrigger
  module Repository
    # This TriggerRepository is class for connect to database
    module TriggerRepository
      def self.webhook_list_query_select
        sql = 'SELECT ' + configuration_list + '.id, '
        sql += configuration_list + '.url, '
        sql += configuration_event + '.name '
        sql += webhook_list_query_join
        sql
      end

      def self.webhook_list_query_join
        sql = ' FROM ' + configuration_list
        sql += ' JOIN ' + configuration_event
        sql += ' on ' + configuration_list + '.event_id='
        sql += configuration_event + '.id '
        sql
      end

      def self.webhook_list_query_condition(event, account_id)
        sql = 'WHERE ' + configuration_list
        sql += '.account_id=' + account_id.to_s
        sql += ' AND ' + configuration_event
        sql += ".name like '" + event.to_s + "'"
        sql
      end

      def self.webhook_list(event, account_id)
        sql = webhook_list_query_select
        sql += webhook_list_query_condition(event, account_id)
        results = WebhookTrigger::Repository.client.query(sql)
        results
      end

      def self.webhook_insert_query
        sql = 'INSERT INTO ' + configuration.db + '.'
        sql += configuration.buffer_table
        sql += '(url, event, event_source_id) VALUES '
        sql
      end

      def self.webhook_insert_value(event, url, event_source_id)
        sql = "('" + url.to_s + "', '" + event.to_s + "', "
        sql += event_source_id.to_s + ')'
        sql
      end

      def self.webhook_insert_pending(event, url, event_source_id)
        sql = webhook_insert_query
        sql += webhook_insert_value(event, url, event_source_id)
        WebhookTrigger::Repository.client.query(sql)
      end

      private

      def configuration_list
        configuration_list.list
      end

      def configuration_event
        configuration.event
      end

      def configuration
        @configuration ||= WebhookTrigger.configuration
      end
    end
  end
end
