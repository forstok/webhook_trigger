# frozen_string_literal: true

module WebhookTrigger
  # This Configuration class is for initialize config to database
  class Configuration
    attr_accessor :db, :host, :port, :username, :password, :buffer_table,
                  :list, :event
  end
end
