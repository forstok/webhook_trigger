# frozen_string_literal: true

module WebhookTrigger
  # Class Trigger is main class for run perform
  module Repository
    def self.client
      Mysql2::Client.new(
        host: configuration.host,
        port: configuration.port,
        username: configuration.username,
        password: configuration.password,
        database: configuration.db
      )
    end

    def self.configuration
      @configuration ||= WebhookTrigger.configuration
    end
  end
end
