# frozen_string_literal: true

require 'webhook_trigger/repository/trigger_repository.rb'
require 'byebug'
module WebhookTrigger
  # Class Trigger is main class for run perform
  module Trigger
    def self.perform(event, event_source_id, account_id)
      webhooks = get_webhooks(event, account_id)
      webhooks.each do |webhook|
        WebhookTrigger::Repository::TriggerRepository
          .webhook_insert_pending(
            webhook['name'],
            webhook['url'],
            event_source_id
          )
      end
    end

    def self.get_webhooks(event, account_id)
      WebhookTrigger::Repository::TriggerRepository
        .webhook_list(event, account_id)
    end
  end
end
