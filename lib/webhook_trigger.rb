# frozen_string_literal: true

require 'webhook_trigger/version'
require 'webhook_trigger/trigger.rb'

# WebhookTrigger is main module
module WebhookTrigger
  class Error < StandardError; end
  # Your code goes here...

  def self.configuration
    @configuration ||= Configuration.new
  end

  def self.reset
    @configuration = Configuration.new
  end

  def self.configure
    yield(configuration)
  end
end
